#ifndef FUNCTIONS
#define FUNCTIONS
#include <vector>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <sys/types.h>
#include <dirent.h>
#include <errno.h>
#include <string>
#include <sys/stat.h>
#include <unistd.h>
#include <time.h>
#include <sstream>
#include <sys/stat.h>
#include <map>
#include <sys/wait.h>


#define USE_COPY  0x01 // << 3 = 0x01
#define DEL_COPY  0x02 // << 2 = 0xf000;
#define DEL_EXTRA 0x04 // << 1 = 0xf000;
#define OVERWRITE 0x08 // << no shift needed.
#define BUFFER_SIZE 64
#define MAX_LOOPS 5
//0000 1111 1111 1111
//0000 0000 1111 1111

//bitwise or would be.
//0000 1111 1111 1111

//0000 0000 0000 1111

//bitwise or would be.
//0000 1111


using namespace std;

namespace func {

class Functions {
public:
    int args = 0x0;
    struct date {
        string year;
        string month;
        string day;
        string hour;
        string minute;
        string second;
    };
    map<string, bool>extensions = map<string,bool>();
    //this shouldn't change.
    const string folder_name = "recup_dir";

    //assigned at launch.
    string destination_folder = "";
    //asigned at launch.
    string source_folder = "";
    //asigned at launch.
    //bool deletefiles
    int file_count = 0;
    int folder_number = 0;
    const vector<string> extension_names = {"png","jpg","jpeg","bmp","ico",
                                            "psd","gif","arw","cr2","crw",
                                            "dcm","djvu","dng","fpx","jp2",
                                            "nef","orf","pcd","pcx","pict",
                                            "sfw","tga","tif","tiff","webp",
                                            "xcf","ai","cdr","emz","odg",
                                            "svg","vsd","vmf","wpg","mov",
                                            "mp4","flv","m4v","mpeg","mpg",
                                            "mpe","avi","ogg","3gp","3gpp",
                                            "asf","divx","fv4","h264","ifo",
                                            "m2ts","mkv","mod","mswmm","mts",
                                            "mxf","ogv","rm","swf","ts",
                                            "vep","vob","webm","wimp","wmv",
                                            "3g2",};

    //default constructor.
    Functions(){

    }

    date getFileDate(string &url){
        struct tm* clock;				// create a time structure

        struct stat attrib;			// create a file attribute structure

        stat(url.c_str(), &attrib);		// get the attributes of afile.txt

        clock = gmtime(&(attrib.st_mtime));	// Get the last modified time and put it into the time structure



        // the time.
         int year = clock->tm_year; //returns the year (since 1900)

         int month = clock->tm_mon; //returns the month (January = 0)

         int day = clock->tm_mday; //returns the day of the month

         int hour = clock->tm_hour;

         int minute = clock->tm_min;

         int second = clock->tm_sec;

         //our date structure.
         date mdate;
         mdate.year = to_string(year + 1900);
         mdate.month = intToString(month + 1, 2);
         mdate.day = intToString(day, 2);
         mdate.hour = intToString(hour, 2);
         mdate.minute = intToString(minute, 2);
         mdate.second = intToString(second, 2);
         return  mdate;

    }

    string intToString(int i, int length){
        string s = to_string(i);
        int dif = length - s.length();
        while(dif > 0){
            s = "0" + s;
            --dif;
        }
        return s;
    }

    void beginSort(){
        //create an empty vector.
        vector<string>folders = vector<string>();

        //populate it with the folders / files.
        if(!listFiles(source_folder, folders)){
            cout << "We've encoutered an error. Please contact support!" << endl;
            return;
        }

        //now sort the folders.
        sortFolders(folders);

    }

    void beginCompare(){
        vector<string>dirs;
        if(args & USE_COPY && !(args & DEL_COPY))
            args &= ~USE_COPY;

        listFiles(destination_folder, dirs);
        compareFolders(dirs, destination_folder);
    }

    bool listFiles(string dir, vector<string> &files){
        //directory pointer.
        DIR *dp;
        struct dirent *dirp;
        if((dp  = opendir(dir.c_str())) == NULL) {
            cout << "Error(" << errno << ") opening " << dir << endl;
            return false;
        }

        //loop through the pointer..
        while ((dirp = readdir(dp)) != NULL) {
            files.push_back(string(dirp->d_name));
        }
        //close the direcotry...
        closedir(dp);
        return true;

    }

    void sortFolders(vector<string>dirs){

        //loop through the files.
        for(string dir : dirs){
            dir = source_folder + "/" + dir;



            //check to see if it's one that we want to open.
            if(!containsPhrase(folder_name, dir))
                continue;

            //now that we know it exists we need to check what number it is.
            string num = getExtension(dir);
            //cout << "Num = " << num << endl;
            int number = atoi(num.c_str());

            //cout << "Number = " << number;

            if(number == 0)
                continue;

            //if the folder_number is equal to zero..
            folder_number = number;





            //now that we've checked a few things we can try opening this directory.
            vector<string> files = vector<string>();

            //try listing the files in the directory.
            if(!listFiles(dir, files))
                continue;

            //cout << "We're now going to sort the files." << endl;
            cout << endl << "Sorting folder: " << dir << endl;
            //now that we were able to list the files, let's sort them.
            sortFiles(files, dir);

            //now delete the sorted folder (if this option is selected).

            if(args & DEL_EXTRA && (args & DEL_COPY || !(args & USE_COPY)) && rmdir(string(dir).c_str()) != 0)
                printf("Error deleting folder: %s\n at: %s\n", strerror(errno), string(dir).c_str());


        }

    }

    void sortFiles(vector<string>files, const string dir){
        //this will be very similar to the sortFolders function.
        string ext;
        //cout << "Sorting files.." << endl;
        //loop through the files.
        for(string file : files){
            ext = getExtension(file);

            //cout << "File name = " << file << endl;
            cout << ".";

            //check to see if the extension was valid.
            if(ext == "")
                continue;

            //using our contains function let's do some sorting.
            if(!extensions[ext]){

                if(args & DEL_EXTRA && remove(string(dir + "/" + file).c_str()) != 0)
                    printf("Error removing file: %s\n from: %s\n", strerror(errno), string(dir + "/" + file).c_str());

                continue;
            }
            //move the file to it's new home.
            string source_url = dir + "/" + file;
            date file_date = getFileDate(source_url);
            string file_name =  "file_" + intToString(file_count, 4) + "_" + file_date.year + "-" + file_date.month + "-" + file_date.day + "." + ext;
            int max = MAX_LOOPS;
            moveFile(source_url, destination_folder + "/" + file_date.year + "/" + ext + "/" + file_name, max);
        }
    }

    void compareFolders(const vector<string>dirs, const string directory){
        cout << endl << "Comparing directory " << directory << " " << dirs.size() << " files located." << endl;
        for(string dir : dirs){
            cout << ".";
            if(dir.length() == 0 || dir[0] == '.' || dir == "duplicates")
                continue;

            string absolutedir = directory + "/" + dir;

            //if the file is a directory, do the same.
            if(isDirectory(absolutedir)){
                vector<string>folders;
                listFiles(absolutedir, folders);
                compareFolders(folders, absolutedir);
                continue;
            }


            cout << "Comparing file " << absolutedir << " with " << dirs.size() << " other files. " << endl;
            for(string file : dirs){
                //cout << ".";
                if(file.length() == 0 || file == dir)
                    continue;

                string absolutefile = directory + "/" + file;
                if(compare(absolutedir.c_str(), absolutefile.c_str())){

                 string destination = directory + "/duplicates/" + file;

                 //cout << endl <<  "Duplicate found! " << endl;

                 int max = MAX_LOOPS;
                 moveFile(absolutefile, destination, max);
              }

            }

        }
    }


    string getFilename (const string& str)
    {
      std::size_t found = str.find_last_of("/\\");
      if(found != string::npos && (found + 1) < str.length())
          return str.substr(found+1);
      else
          return "null.null";
    }
    string getExtension(const string& str){
        std::size_t found = str.find_last_of(".");
        if(found != string::npos && (found + 1) < str.length())
            return str.substr(found+1);
        else
            return "";
    }

    bool containsPhrase(string phrase, string body){
        std::size_t found = body.find(phrase);
          if (found!=std::string::npos)
              return true;
          else
              return false;
    }

    //function for moving files.
    bool moveFile(string source, string destination, int& max_num){
        if(max_num < 1)
            return false;

        --max_num;

        //cout << " Source = " << source << endl;
        //cout << " Destination = " << destination << endl;
        int result;
        //add one to the number of files copied.

        if(getExtension(source).length() == 0 || getExtension(destination).length() == 0)
            return false;

        //create char arrays for the source and destination.
        const char* oldname = source.c_str();

        //the verify function checks to see if the file exists and if it does we cycle through our file
        //count til we reach a name that does not exist.
        const char* newname;
        if(args & OVERWRITE)
            newname = destination.c_str();
        else{
            newname = verify(destination.c_str());
             //this is a last precaution. We check to see if the file exists again.
            ifstream check;
            check.open(newname);
            if(check.good()){
                return false;
            }

        }

        //depending on the result, display different text.
        if(args & USE_COPY)
            result= copy( oldname , newname );
        else
            result= rename( oldname , newname );

        if ( result == 0 ){
             //cout << "File #" << file_count << " moved successfully!" << endl;
            ++file_count;
            return true;
        } else {
            printf("Error moving file: %s\n from: %s\n to: %s\n", strerror(errno), oldname, newname);
        }



        //cout << endl << "Error moving file " << newname << endl << " with destination " << destination << endl;

        //make the directories & try again.
        makeDirs(destination);

        //return the results..
        return moveFile(source, destination, max_num);
    }
    int copy(const char* source, const char* destination){
\
        //cout << " Running copy " << endl;
        //now we open a input stream from the source file.
        ifstream sfile;
        sfile.open(source);
        // and an output stream to the output file.
        ofstream dfile;
        dfile.open(destination);

        //verify that both streams are good.
        if(!sfile.is_open() || !dfile.is_open())
            return 1;
        char c;
        //copy the data one byte at a time :)
        while(sfile.get(c))
                dfile.put(c);

        //close the files.
        sfile.close();
        dfile.close();

        if(args & DEL_COPY && remove(source) != 0)
            printf("Error removing file: %s\n from: %s\n", strerror(errno), source);


        return 0;

    }

    const char* verify(const char* name){

        string newname(name);
        while(fileExists(newname)){
            ++file_count;
            size_t end = newname.find_last_of('_');
            size_t start = string::npos;
            //cout << newname << endl;
            if(end != string::npos && end > 9 && end < newname.length())
                start = newname.find('_', end - 10);

            if(end == string::npos || end >= newname.length() || start == string::npos || start >= newname.length()){
                //newname = "file_" + intToString(file_count, 4) + "_" + newname;
                string name = getFilename(newname);
                newname = newname.substr(0, (newname.length() - name.length()) - 1) + "/file_" + intToString(file_count, 4) + "_" + name;
                //cout << " NEW NAME = " << newname << endl;
                continue;
            }

            string ending = newname.substr(end);
            string starting = newname.substr(0, start + 1);
            newname = starting + intToString(file_count, 4) + ending;
        }
        return newname.c_str();

    }


    bool compare(const char* file_a, const char* file_b){
        //open the two streams.
        ifstream a;
        a.open(file_a);
        ifstream b;
        b.open(file_b);

        //if they're both good preceed.
        if(!a.is_open() || !b.is_open())
            return false;

        char ac;
        char bc;

        //compare one byte at a time :)
        while(a.get(ac)){
            b.get(bc);
            if(ac != bc)
                return false;
        }

        if(b.get(bc))
            return false;
        else
            return true;

    }



    void makeDirs(string url){
        //split the url..
        vector<string> folders = splitString(url, '/');
        string current_url = "";
        //loop through each folder & try to create it.
        folders.pop_back();
        for(string folder : folders){
            if(folder.length() < 2)
                continue;

            current_url = current_url + "/" + folder;

            //cout << "Current URL = " << current_url << endl;

            if(mkdir(current_url.c_str(), 0775) != 0)
                printf("Error creating directory: %s\n at: %s\n", strerror(errno), current_url.c_str());

        }

    }

    bool fileExists(const std::string& filename)
    {
        struct stat buf;
        return stat(filename.c_str(), &buf) != -1;

    }
    bool isDirectory(const std::string& filename){
        struct stat s;
        if( stat(filename.c_str(),&s) == 0 )
        {
            if( s.st_mode & S_IFDIR )
            {
                return true;
            }
            else if( s.st_mode & S_IFREG )
            {
                //it's a file
                return false;
            }
            else
            {
                //something else
                return false;
            }
        }
        else
        {

            //error
            return false;
        }
        return false;
    }

    vector<string>splitString(const string body, const char where){
        vector<string>strings = vector<string>();
        istringstream ss(body);
        string token;
            while(std::getline(ss, token, where)) {
                strings.push_back(token);
                //std::cout << token << '\n';
            }

        return strings;
    }

    //simple welcome screen.
    void welcome(){
        string default_destination = string(getenv("HOME")) + "/Pictures/Recovered";
        string default_source = getenv("HOME");
        string name = getFilename(default_source);
        name[0] = toupper(name[0]);
        string answer;
        string arguments;
        bool loop = true;

        //add extensions to map.
        for(string extension : extension_names){
            //loop through each extension & compare it with our file's extension.
             extensions[extension] = true;

        }

        cout << endl << endl << endl << endl << "Hi " + name + ", Welcome to the program! " << endl << endl << "We'll need you to do a few things before we can get started." << endl;
        cout << "This program could take a while to complete so be patient!" << endl << endl;

        cout << "What would you like to do? " << endl << "A: Sort recup.dir files " << endl << "B: Remove duplicates" << endl << "Type A or B and press enter:";
        cin >> answer;
        cout << endl << " Perfect! Now we have a few more options for you. \n A: Copy files instead of moving \n B: Delete sorted folders \n C: Delete file after copying \n D: Overwite Existing files" << endl;
        cout << endl << "Select one of the following or press any other key and push enter to finish setup.";
        while(loop){
            cout << endl << "Option: ";
            cin >> arguments;
            switch(toupper(arguments[0])){
            case 'A':
                args |= USE_COPY;
                cout << endl << "Option A added to config!";
                break;
            case 'B':
                args |= DEL_EXTRA;
                cout << endl << "Option B added to config!";
                break;
            case 'C':
                args |= DEL_COPY;
                cout << endl << "Option C added to config!";
                break;
            case 'D':
                args |= OVERWRITE;
                cout << endl << "Option D added to config!";
                break;
            default:
                loop = false;
                cout << endl << "Okay, we'll precceed to the next step!";
            }
        }

        if(toupper(answer[0]) == 'A'){


        cout << endl << endl << "Please enter the destination folder path. The default destination path is " << default_destination << endl << "(type D and press enter to leave as default)" << endl << "Destination Folder: ";
        cin >> destination_folder;
        if(destination_folder.length() < 5)
            destination_folder = default_destination;

        cout << endl << "Excelent! Now enter the source folder path. \n Below is a list of external drive folders. " << endl << endl;
        vector<string>drives;
        listFiles("/Volumes", drives);
        for(string drive : drives){
            if(drive[0] != '.')
            cout << "/Volumes/" << drive << endl;

        }
        cout << endl << " The default is " << default_source << endl << "(type D and press enter to leave as default) " << endl << "Source Folder: ";
        cin >> source_folder;
        if(source_folder.length() < 5)
            source_folder = default_source;

        cout << endl << endl << "Good job! You've completed the config. " << endl << "Are you sure you want to continue? (y/n) ";

        cin >> answer;
        if(toupper(answer[0]) == 'Y'){

            cout << endl << "Sorting begun.." << endl;

            beginSort();

        cout << endl << endl << "File sorting completed!" << endl << endl;
        } else {
            cout << "That's okay. You can always use this program again!" << endl;
            exit(0);
        }

    } else if(toupper(answer[0]) == 'B'){
            cout << "Please enter the folder you wish to find duplicates in. The default path is " << default_destination << endl << "(type D and press enter to leave as default)" << endl << "Compare Directory: ";
            cin >> destination_folder;
            if(destination_folder.length() < 5)
                destination_folder = default_destination;

            cout << endl << "Excellent! You've completed the config. " << endl << "Are you sure you want to continue? (y/n) ";
            cin >> answer;
            if(toupper(answer[0]) == 'Y'){

                cout << endl << "Finding duplicates.." << endl;

                beginCompare();

            cout << endl << endl << "File sorting completed!" << endl << endl;
            } else {
                cout << "That's okay. You can always use this program again!" << endl;
                exit(0);
            }
        }
    }
    string getStringFromConsole(){
        string value = "";
        while(true){
            cin >> value;
            if(value.length() < 2)
                cout << "You'll have to try a bit harder i'm afraid.." << endl;
            else
                return value;

        }
    }


private:
};

}

#endif // FUNCTIONS

