/* rename example */
#include <stdio.h>
#include <iostream>
#include "functions.h"
#include <regex>

using namespace func;
using namespace std;

void getRoot(const char* name){
    if (getuid() != 0){
       cout << " Welcome to the program! For us to process the files and folders we need permissions. \n Please type your password and press enter: ";
       system(name);
       exit(0);
    } else {
        Functions functions = Functions();
        functions.welcome();
    }

}
int main (int args, char** argv)
{


    //Functions().welcome();
    //cout << argv[0];
    string command;

    command += argv[0];

    std::regex mclean("\\s");
    command = std::regex_replace(command, mclean, "\\ ");

    command = "sudo " + command;

    //cout << " Name = " << command;
    getRoot(command.c_str());
  return 0;
}
